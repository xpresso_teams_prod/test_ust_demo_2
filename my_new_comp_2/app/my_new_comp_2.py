import os
import sys
import json
import types
import pickle
import marshal
import datetime
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow_hub as hub
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType
## $xpr_param_component_name = my_new_comp_2
## $xpr_param_component_type = pipeline_job 
## $xpr_param_global_variables = []
## $xpr_param_global_methods = []
questions = df['Important Questions'].tolist()
embeddings = embed(questions)
type(embeddings)
embeddings
#Request
Plan_Name = "Ambetter Platinum 1"
Plan_Type = "HMO"
query = "out of pcoket"
start = datetime.datetime.now()
index_list = df.index[(df['Plan_Name'] == Plan_Name) & (df['Plan_Type'] == Plan_Type)].tolist()
query_embed = embed([query])
similarity = np.inner(query_embed,embeddings)
max_index = np.argmax(similarity[:,index_list], axis = 1)
df_index = index_list[max_index[0]]
end = datetime.datetime.now()
diff = end - start
elapsed_ms = (diff.days * 86400000) + (diff.seconds * 1000) + (diff.microseconds / 1000)
print("Query processed in time in milliseconds :", elapsed_ms)
print("Plan_Name : ",df.loc[df_index]['Plan_Name'])
print("Plan_Type : ",df.loc[df_index]['Plan_Type'])
#print("Question : ",df.loc[df_index]['Important Questions'])
print("Answers : ",df.loc[df_index]['Answers'])
#print("Why this Matters: : ",df.loc[df_index]['Why this Matters:'])
print("Url: ",df.loc[df_index]['Url'])
